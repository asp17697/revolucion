new Vue({
  el: '#ProductoList',
  delimiters: ['${','}'],
  data: {
    productoList: [],
    currentProducto: {},
    search_term: '',
    loading: true,
  },
  methods: {
    getProductoList: function() {
      let api_url = '/gestor/api/producto/';
      if(this.search_term !== '' || this.search_term !== null) {
        api_url = api_url.concat('?search='.concat(this.search_term));
      }
      this.$http.get(api_url)
        .then((response) => {
          this.productoList = response.data;
          this.loading = false;
        })
        .catch((err) => {
          this.loading = false;
          console.log(err);
        });
    },
    getProductoUrl: function(producto) {
      return "/gestor/producto/producto_id/".replace('producto_id', producto.id);
    },
  },
  mounted: function() {
    this.getProductoList();
  },
});
