new Vue({
  el: '#CotizacionList',
  delimiters: ['${','}'],
  data: {
    cotizacionList: [],
    currentCotizacion: {},
    search_term: '',
    loading: true,
  },
  methods: {
    getCotizacionList: function() {
      let api_url = '/gestor/api/cotizacion/';
      if(this.search_term !== '' || this.search_term !== null) {
        api_url = api_url.concat('?search='.concat(this.search_term));
      }
      this.$http.get(api_url)
        .then((response) => {
          this.cotizacionList = response.data;
          this.loading = false;
        })
        .catch((err) => {
          this.loading = false;
          console.log(err);
        });
    },
    getContactoNombre: function(cotizacion) {
      if(cotizacion.contacto === null) {
        return '';
      }
      return cotizacion.contacto.nombre;
    },
    getUsuarioUsername: function(cotizacion) {
      if(cotizacion.usuario === null) {
        return '';
      }
      return cotizacion.usuario.username;
    },
    getEmpresaRazonSocial: function(cotizacion) {
      this.$http.get('/gestor/api/empresa/'.concat(cotizacion.empresa))
        .then((response) => {
          this.loading = false;
          return response.data.razon_social;
        })
        .catch((err) => {
          this.loading = false;
          console.log(err);
        });
        return '';
    },
    getCotizacionUrl: function(cotizacion) {
      return "/gestor/empresa/empresa_rfc/cotizacion/cotizacion_id/".replace('empresa_rfc', cotizacion.empresa.rfc).replace('cotizacion_id', cotizacion.id);
    },
    getCotizacionEmpresaUrl: function(cotizacion) {
      return "/gestor/empresa/empresa_rfc/".replace('empresa_rfc', cotizacion.empresa.rfc);
    },
    getCotizacionContactoUrl: function(cotizacion) {
      if(cotizacion.contacto === null) {
        return '';
      }
      return "/gestor/empresa/empresa_rfc/contacto/contacto_id/".replace('empresa_rfc', cotizacion.empresa.rfc).replace('contacto_id', cotizacion.contacto.id);
    },
  },
  mounted: function() {
    this.getCotizacionList();
  },
});
