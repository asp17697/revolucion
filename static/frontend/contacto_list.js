new Vue({
  el: '#ContactoList',
  delimiters: ['${','}'],
  data: {
    contactoList: [],
    currentContacto: {},
    search_term: '',
    loading: true,
  },
  methods: {
    getContactoList: function() {
      let api_url = '/gestor/api/contacto/';
      if(this.search_term !== '' || this.search_term !== null) {
        api_url = api_url.concat('?search='.concat(this.search_term));
      }
      this.$http.get(api_url)
        .then((response) => {
          this.contactoList = response.data;
          this.loading = false;
        })
        .catch((err) => {
          this.loading = false;
          console.log(err);
        });
    },
    getContactoUrl: function(contacto) {
      return "/gestor/empresa/empresa_rfc/contacto/contacto_id/".replace('empresa_rfc', contacto.empresa).replace('contacto_id', contacto.id);
    },
    getContactoEmpresaUrl: function(contacto) {
      return "/gestor/empresa/empresa_rfc/".replace('empresa_rfc', contacto.empresa);
    }
  },
  mounted: function() {
    this.getContactoList();
  },
});
