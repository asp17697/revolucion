new Vue({
  el: '#Calculadora',
  delimiters: ['${','}'],
  data: {
    costo: 0.00,
    ganancia: 0.00,
    cantidad: 1.00,
    precio: 0.00,
    margen: 0,
  },
  methods: {
    getPrecio: function(costo) {
      this.ganancia = costo * (this.margen/100);
      this.precio = Number(costo + this.ganancia).toFixed(2);
      return Number(costo + this.ganancia).toFixed(2);
    },
    getImporte: function() {
      return this.precio * this.cantidad;
    },
  }
});

new Vue({
  el: '#BuscarProvedor',
  delimiters:['${', '}'],
  data: {
    empresaList: [],
    currentEmpresa: {},
    currentEmpresaRFC: '',
    search_term: '',
    loading: false,
  },
  methods: {
    getEmpresaList: function(){
      this.loading = true;
      let api_url = '/gestor/api/empresa/';
      if(this.search_term !== '' || this.search_term !== null) {
        api_url = api_url.concat('?search='.concat(this.search_term));
      }
      this.$http.get(api_url)
        .then((response) => {
          this.empresaList = response.data;
          this.loading = false;
        })
        .catch((err) => {
          this.loading = false;
          console.log(err);
        });
    },
    getEmpresa: function(rfc){
      this.loading = true;
      console.log('/gestor/api/empresa/empresa_rfc/'.replace('empresa_rfc', rfc));
      this.$http.get('/gestor/api/empresa/empresa_rfc/'.replace('empresa_rfc', rfc))
        .then((response) => {
          this.currentEmpresa = response.data;
          this.currentEmpresaRFC = this.currentEmpresa.rfc;
          this.loading = false;
        })
        .catch((err) => {
          this.loading = false;
          console.log(err);
        });
    },
  }
});
