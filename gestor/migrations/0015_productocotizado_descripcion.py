# Generated by Django 2.0.6 on 2018-07-10 04:13

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('gestor', '0014_auto_20180710_0352'),
    ]

    operations = [
        migrations.AddField(
            model_name='productocotizado',
            name='descripcion',
            field=models.TextField(default='sin descripción', max_length=55),
            preserve_default=False,
        ),
    ]
