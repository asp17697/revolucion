# Generated by Django 2.0.6 on 2018-07-06 23:24

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('gestor', '0009_producto_descripcion'),
    ]

    operations = [
        migrations.AddField(
            model_name='productocotizado',
            name='monto_iva',
            field=models.DecimalField(decimal_places=2, default=0.0, max_digits=10),
        ),
    ]
