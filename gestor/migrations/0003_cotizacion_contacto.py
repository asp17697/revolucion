# Generated by Django 2.0.6 on 2018-07-04 03:39

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('gestor', '0002_auto_20180704_0334'),
    ]

    operations = [
        migrations.AddField(
            model_name='cotizacion',
            name='contacto',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='gestor.Contacto'),
        ),
    ]
