from rest_framework import serializers

from django.contrib.auth.models import User

from .models import Empresa
from .models import Contacto
from .models import Producto
from .models import Cotizacion
from .models import ProductoCotizado
from .models import Factura

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = '__all__'

class UserEssentialDataSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['username', ]

class EmpresaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Empresa
        fields = ['rfc', 'razon_social']

class ContactoSerializer(serializers.ModelSerializer):
    empresa = EmpresaSerializer(read_only=True)

    class Meta:
        model = Contacto
        fields = ['id', 'empresa', 'nombre']

class ProductoSerializer(serializers.ModelSerializer):
    empresa = EmpresaSerializer

    class Meta:
        model = Producto
        fields = '__all__'

class FacturaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Factura
        fields = '__all__'

class CotizacionSerializer(serializers.ModelSerializer):
    productos_cotizados = serializers.PrimaryKeyRelatedField(many=True, read_only=True)
    factura = FacturaSerializer(read_only=True)
    empresa = EmpresaSerializer(read_only=True)
    contacto = ContactoSerializer(read_only=True)
    usuario = UserEssentialDataSerializer(read_only=True)

    class Meta:
        model = Cotizacion
        fields = [
            'id', 'factura', 'referencia', 
            'empresa', 'is_factura', 'contacto', 
            'usuario', 'fecha', 'productos_cotizados'
        ]

class ProductoCotizadoSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProductoCotizado
        fields = ['id', 'cotizacion', 'producto', 
            'costo', 'margen_ganancia', 'provedor',
        ]
