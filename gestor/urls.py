from django.urls import path, include

from . import routers
from . import views

from django.contrib.auth import views as auth_views

app_name = 'gestor'

urlpatterns = [
    path('', views.HomeView.as_view(), name='home'),
    path('signup/', views.SignUpView.as_view(), name='signup'),
    path('login/', auth_views.LoginView.as_view(), name='login'),
    path('logout/', auth_views.LogoutView.as_view(template_name='gestor/base.html'), name='logout'),
]


urlpatterns += [
    path('empresa/', views.EmpresaCreate.as_view(), name='create_empresa'),
    path('empresa/<slug:rfc>/', views.EmpresaContactoList.as_view(), name='detail_empresa'),
    path('empresa/<slug:rfc>/actualizar/', views.EmpresaUpdate.as_view(), name='update_empresa'),
    path('empresa/<slug:rfc>/eliminar/', views.EmpresaDelete.as_view(), name='delete_empresa'),
    path('empresas/', views.EmpresaList.as_view(), name='list_empresa'),
]

urlpatterns += [
    path('contactos/', views.ContactoList.as_view(), name='list_contacto'),
    path('empresa/<slug:rfc>/contactos/', views.EmpresaContactoList.as_view(), name='list_contacto_empresa'),
    path('empresa/<slug:rfc>/contacto/', views.ContactoCreate.as_view(), name='create_contacto'),
    path('empresa/<slug:rfc>/contacto/<int:id>/', views.ContactoDetail.as_view(), name='detail_contacto'),
    path('empresa/<slug:rfc>/contacto/<int:id>/actualizar/', views.ContactoUpdate.as_view(), name='update_contacto'),
    path('empresa/<slug:rfc>/contacto/<int:id>/eliminar/', views.ContactoDelete.as_view(), name='delete_contacto'),
]

urlpatterns += [
    path('productos/', views.ProductoList.as_view(), name='list_producto'),
    path('producto/', views.ProductoCreate.as_view(), name='create_producto'),
    path('producto/<int:id>/', views.ProductoUpdate.as_view(), name='detail_producto'),
    path('producto/<int:id>/actualizar/', views.ProductoUpdate.as_view(), name='update_producto'),
    path('producto/<int:id>/eliminar/', views.ProductoDelete.as_view(), name='delete_producto'),
]

urlpatterns += [
    path('cotizaciones/', views.CotizacionList.as_view(), name='list_cotizacion'),
    path('empresa/<slug:rfc>/cotizaciones/', views.EmpresaCotizacionList.as_view(), name='list_cotizacion_empresa'),
    path('empresa/<slug:rfc>/cotizacion/', views.CotizacionCreate.as_view(), name='create_cotizacion'),
    path('empresa/<slug:rfc>/cotizacion/<int:pk>/', views.CotizacionDetail.as_view(), name='detail_cotizacion'),
    path('empresa/<slug:rfc>/cotizacion/<int:pk>/pdf/', views.CotizacionGeneratePDF.as_view(), name='generate_pdf_cotizacion'),
    path('empresa/<slug:rfc>/cotizacion/<int:pk>/actualizar/', views.CotizacionUpdate.as_view(), name='update_cotizacion'),
    path('empresa/<slug:rfc>/cotizacion/<int:pk>/producto_cotizado/<int:id>/eliminar/', views.ProductoCotizadoDelete.as_view(), name='delete_prodcuto_cotizado'),
    path('empresa/<slug:rfc>/cotizacion/<int:pk>/producto_cotizado/<int:id>/actualizar/', views.ProductoCotizadoUpdate.as_view(), name='update_producto_cotizado'),
    path('empresa/<slug:rfc>/cotizacion/<int:pk>/producto/<int:id>/agregar/', views.ProductoAdd.as_view(), name='add_producto'),
]

urlpatterns += [
    path('empresa/<slug:rfc>/facturas/', views.EmpresaFacturaList.as_view(), name='list_factura_empresa'),
]

urlpatterns += [
    path('empresa/<slug:rfc>/cotizacion/<int:pk>/facturar/', views.FacturaCreate.as_view(), name='create_factura'),
    path('empresa/<slug:rfc>/factura/<int:id>/', views.FacturaDetail.as_view(), name='detail_factura'),
]

urlpatterns += [
    path('api/', include(routers.router.urls)),
]
