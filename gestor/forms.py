from django.forms import ModelForm

from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm

from .models import Empresa
from .models import Contacto
from .models import Producto
from .models import Cotizacion
from .models import ProductoCotizado
from .models import Factura

class EmpresaCreateForm(ModelForm):
    class Meta:
        model = Empresa
        fields = '__all__'
        labels = {
            'rfc': 'RFC',
            'cp': 'CP',
            'cuidad_estado': 'Cuidad/Estado',
            'direccion':  'Dirección',
            'forma_pago': 'Forma de pago',
            'metodo_pago': 'Método de pago',
            'tipo_comprobante': 'Tipo de comprobante',
            'razon_social': 'Razón social',
        }

class EmpresaUpdateForm(ModelForm):
    class Meta:
        model = Empresa
        exclude = ['rfc']
        labels = {
            'cp': 'CP',
            'cuidad_estado': 'Cuidad/Estado',
            'direccion':  'Dirección',
        }

class ContactoCreateForm(ModelForm):
    class Meta:
        model = Contacto
        exclude = ['empresa']

class CotizacionCreateForm(ModelForm):
    class Meta:
        model = Cotizacion
        fields = ['referencia', 'contacto', 'costo_operacion']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['contacto'].empty_label = "(Contacto)"

class CotizacionUpdateForm(ModelForm):
    class Meta:
        model = Cotizacion
        fields = ['referencia', 'contacto', 'costo_operacion']
        labels = {
            'costo_operacion': 'Costo de operación',
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['contacto'].empty_label = "(Contacto)"

class ProductoCotizadoUpdateForm(ModelForm):
    class Meta:
        model = ProductoCotizado
        fields = ['costo', 'cantidad', 'provedor', 'margen_ganancia', 'descripcion']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['provedor'].empty_label = "(Provedor)"

class ProductoAddForm(ModelForm):
    class Meta:
        model = ProductoCotizado
        fields = ['costo', 'cantidad', 'provedor', 'margen_ganancia', 'descripcion']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['provedor'].empty_label = "(Provedor)"

class FacturaCreateForm(ModelForm):
    class Meta:
        model = Factura
        fields = ['firma']

class FacturaUpdateForm(ModelForm):
    class Meta:
        model = Factura
        fields = ['monto_pagado']

class ProductoCreateForm(ModelForm):
    class Meta:
        model = Producto
        fields = '__all__'
        labels = {
            'tipo_iva': 'Tipo de IVA',
            'tipo_producto': 'Tipo de producto',
            'unidad_medida': 'Unidad de medida',
            'descripcion': 'Descripción',
            'numero_parte': 'Numero de parte',
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['clave'].empty_label = "(Clave)"

class UserSignUpForm(UserCreationForm):
    class Meta:
        model = User
        fields = ['username', 'first_name', 'last_name', 'email']
