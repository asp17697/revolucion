from rest_framework import viewsets

from rest_framework.filters import SearchFilter
from django_filters.rest_framework import DjangoFilterBackend

from django.contrib.auth.models import User
from .serializers import UserSerializer

from .models import Empresa
from .serializers import EmpresaSerializer

from .models import Contacto
from .serializers import ContactoSerializer

from .models import Producto
from .serializers import ProductoSerializer

from .models import Cotizacion
from .serializers import CotizacionSerializer

from .models import ProductoCotizado
from .serializers import ProductoCotizadoSerializer

from .models import Factura
from .serializers import FacturaSerializer

class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer

class EmpresaViewSet(viewsets.ModelViewSet):
    queryset = Empresa.objects.all()
    serializer_class = EmpresaSerializer
    filter_backends = (SearchFilter,)
    search_fields = ('rfc', 'razon_social',)

class ContactoViewSet(viewsets.ModelViewSet):
    queryset = Contacto.objects.all()
    serializer_class = ContactoSerializer
    filter_backends = (DjangoFilterBackend, SearchFilter,)
    filter_fields = ('empresa',)
    search_fields = (
        'id', 'nombre', 
        'empresa__rfc', 'empresa__razon_social',
    )

class EmpresaContactoViewSet(viewsets.ModelViewSet):
    serializer_class = ContactoSerializer
    lookup_field = 'rfc'

    def get_queryset(self):
        empresa = Empresa.objects.get(rfc=self.kwargs['rfc'])
        return Contacto.objects.filter(empresa=empresa)

class ProductoViewSet(viewsets.ModelViewSet):
    queryset = Producto.objects.all()
    serializer_class = ProductoSerializer
    filter_backends = (SearchFilter,)
    search_fields = ('id', 'numero_parte',)

class EmpresaProductoViewSet(viewsets.ModelViewSet):
    serializer_class = ProductoSerializer
    lookup_field = 'rfc'

    def get_queryset(self):
        empresa = Empresa.objects.get(rfc=self.kwargs['rfc'])
        return Producto.objects.filter(empresa=empresa)

class CotizacionViewSet(viewsets.ModelViewSet):
    queryset = Cotizacion.objects.all()
    serializer_class = CotizacionSerializer
    filter_backends= (DjangoFilterBackend, SearchFilter,)
    filter_fields = ('empresa', 'contacto', 'usuario', 'is_factura',)
    search_fields = (
        'id', 'referencia', 'fecha', 'is_factura',
        'empresa__rfc', 'empresa__razon_social', 
        'usuario__username',
        'contacto__nombre',
    )

class ProductoCotizadoViewSet(viewsets.ModelViewSet):
    serializer_class = ProductoCotizadoSerializer
    queryset = ProductoCotizado.objects.all()
    filter_backends = (DjangoFilterBackend,)
    filter_fields = ('cotizacion', 'producto__numero_parte')

class FacturaViewSet(viewsets.ModelViewSet):
    serializer_class = FacturaSerializer
    queryset = Factura.objects.all()
    filter_backends = (DjangoFilterBackend, SearchFilter, )
    filter_fields = ('cotizacion', 'estado', )
    search_fields = (
        'estado',
        'cotizacion__id', 'cotizacion__referencia',
        'cotizacion__empresa__rfc', 'cotizacion__empresa__razon_social', 
        'cotizacion__usuario__username',
        'cotizacion__contacto__nombre', 
    )
