from io import BytesIO
from decimal import Decimal

from django.http import HttpResponse
from django.template.loader import get_template

from xhtml2pdf import pisa

def render_to_pdf(template_name, context):
    template = get_template(template_name)
    html = template.render(context)
    response = BytesIO()
    pdf = pisa.pisaDocument(BytesIO(html.encode("UTF-8")), response)
    if not pdf.err:
        return HttpResponse(response.getvalue(), content_type='application/pdf')
    else:    
        return HttpResponse("Error Rendering PDF", status=400)


def update_producto_cotizado(producto_cotizado):
    if not producto_cotizado.producto is None:
        iva = producto_cotizado.producto.tipo_iva
        producto_cotizado.precio = producto_cotizado.costo + Decimal(producto_cotizado.costo) * Decimal(producto_cotizado.margen_ganancia/100)
        producto_cotizado.monto_iva = producto_cotizado.precio * Decimal(iva / 100) * Decimal(producto_cotizado.cantidad)
    else:
        producto_cotizado.precio = producto_cotizado.costo
    producto_cotizado.importe = producto_cotizado.precio * Decimal(producto_cotizado.cantidad)

def update_cotizacion(cotizacion):
    cotizacion.monto_iva = Decimal(0)
    cotizacion.subtotal = Decimal(0)
    cotizacion.total = Decimal(0)
    for producto_cotizado in cotizacion.productos_cotizados.all():
        cotizacion.monto_iva += producto_cotizado.monto_iva
        cotizacion.subtotal += producto_cotizado.importe
    cotizacion.total = cotizacion.monto_iva + cotizacion.subtotal
