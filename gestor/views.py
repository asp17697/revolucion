from django.contrib.auth import authenticate, login
from django.contrib.auth.mixins import LoginRequiredMixin

from django.urls import reverse
from django.template.loader import get_template
from django.http import HttpResponse

from django.views import View
from django.views.generic import ListView
from django.views.generic.base import TemplateView
from django.views.generic.detail import SingleObjectMixin
from django.views.generic.detail import DetailView
from django.views.generic.edit import FormView
from django.views.generic.edit import CreateView
from django.views.generic.edit import UpdateView
from django.views.generic.edit import DeleteView

from .models import Empresa
from .forms import EmpresaCreateForm, EmpresaUpdateForm

from .models import Contacto
from .forms import ContactoCreateForm

from .models import Producto
from .forms import ProductoCreateForm

from .models import Cotizacion
from .forms import CotizacionCreateForm, CotizacionUpdateForm

from .models import ProductoCotizado
from .forms import ProductoAddForm, ProductoCotizadoUpdateForm

from .models import Factura
from .forms import FacturaCreateForm
from .utils import render_to_pdf

from .forms import UserSignUpForm

class HomeView(TemplateView):
    template_name = 'gestor/home.html'

class EmpresaList(TemplateView):
    template_name = 'gestor/empresa/EmpresaList.html'

class EmpresaCreate(CreateView):
    template_name = 'gestor/empresa/EmpresaCreate.html'
    model = Empresa
    form_class = EmpresaCreateForm

    def get_success_url(self, **kwargs):
        return reverse('gestor:detail_empresa', kwargs={'rfc': self.object.rfc })

class EmpresaUpdate(UpdateView):
    template_name = 'gestor/empresa/EmpresaUpdate.html'
    model = Empresa
    form_class = EmpresaUpdateForm
    context_object_name = 'empresa'
    pk_url_kwarg = 'rfc'

    def get_success_url(self, **kwargs):
        return reverse('gestor:detail_empresa', kwargs={'rfc': self.object.rfc})

class EmpresaDelete(DeleteView):
    template_name = 'gestor/empresa/EmpresaDelete.html'
    model = Empresa
    context_object_name = 'empresa'
    pk_url_kwarg = 'rfc'

    def get_success_url(self, **kwargs):
        return reverse('gestor:list_empresa')

class ContactoList(TemplateView):
    template_name = 'gestor/contacto/ContactoList.html'

class EmpresaContactoList(SingleObjectMixin, ListView):
    template_name = 'gestor/contacto/ContactoList(Empresa).html'
    model = Empresa
    pk_url_kwarg='rfc'
    context_object_name = 'empresa'
    paginate_by = 25

    def get(self, request, *args, **kwargs):
        self.object = self.get_object(queryset=Empresa.objects.all())
        return super().get(request, *args, **kwargs)

    def get_queryset(self):
        return self.object.contacto_set.all()

class ContactoCreate(CreateView):
    template_name = 'gestor/contacto/ContactoCreate.html'
    model = Contacto
    form_class = ContactoCreateForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(kwargs)
        context['empresa'] = Empresa.objects.get(rfc=self.kwargs['rfc'])
        return context

    def form_valid(self, form):
        form.instance.empresa = Empresa.objects.get(rfc=self.kwargs['rfc'])
        return super().form_valid(form)

    def get_success_url(self, **kwargs):
        return reverse('gestor:list_contacto_empresa', kwargs={'rfc': self.kwargs['rfc']})

class ContactoDetail(SingleObjectMixin, ListView):
    template_name = 'gestor/contacto/ContactoDetail.html'
    model = Contacto
    pk_url_kwarg='id'
    context_object_name = 'contacto'
    paginate_by = 25

    def get(self, request, *args, **kwargs):
        self.object = self.get_object(queryset=Contacto.objects.all())
        return super().get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['empresa'] = Empresa.objects.get(rfc=self.kwargs['rfc'])
        return context

    def get_queryset(self):
        return self.object.cotizacion_set.all()

class ContactoUpdate(UpdateView):
    template_name='gestor/contacto/ContactoUpdate.html'
    model = Contacto
    form_class = ContactoCreateForm
    context_object_name = 'contacto'
    pk_url_kwarg = 'id'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['empresa'] = Empresa.objects.get(rfc=self.kwargs['rfc'])
        return context

    def get_success_url(self, **kwargs):
        return reverse('gestor:list_contacto_empresa',  kwargs={'rfc': self.kwargs['rfc']})

class ContactoDelete(DeleteView):
    template_name='gestor/contacto/ContactoDelete.html'
    model = Contacto
    context_object_name = 'contacto'
    pk_url_kwarg = 'id'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['empresa'] = Empresa.objects.get(rfc=self.kwargs['rfc'])
        return context

    def get_success_url(self, **kwargs):
        return reverse('gestor:list_contacto_empresa',  kwargs={'rfc': self.kwargs['rfc']})

# Cotización

class CotizacionList(TemplateView):
    template_name = 'gestor/cotizacion/CotizacionList.html'

class EmpresaCotizacionList(SingleObjectMixin, ListView):
    template_name = 'gestor/cotizacion/CotizacionList(Empresa).html'
    model = Empresa
    pk_url_kwarg='rfc'
    context_object_name = 'empresa'
    paginate_by = 25

    def get(self, request, *args, **kwargs):
        self.object = self.get_object(queryset=Empresa.objects.all())
        return super().get(request, *args, **kwargs)

    def get_queryset(self):
        return self.object.cotizaciones.all()

class CotizacionCreate(CreateView):
    template_name = 'gestor/cotizacion/CotizacionCreate.html'
    form_class = CotizacionCreateForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['empresa'] = Empresa.objects.get(rfc=self.kwargs['rfc'])
        return context

    def form_valid(self, form):
        form.instance.empresa = Empresa.objects.get(rfc=self.kwargs['rfc'])
        form.instance.usuario = self.request.user
        return super().form_valid(form)

    def get_success_url(self, **kwargs):
        cotizacion_kwargs = {
            'rfc': self.kwargs['rfc'],
            'pk': self.object.pk,
        }
        return reverse('gestor:detail_cotizacion', kwargs=cotizacion_kwargs)


class CotizacionDetail(DetailView):
    template_name = 'gestor/cotizacion/CotizacionDetail.html'
    model = Cotizacion
    context_object_name = 'cotizacion'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['empresa'] = Empresa.objects.get(rfc=self.kwargs['rfc'])
        context['producto_cotizado_list'] = self.object.productos_cotizados.all()
        context['factura'] = None
        if self.object.is_factura:
            context['factura'] = self.object.factura
        return context

class CotizacionUpdate(UpdateView):
    template_name = 'gestor/cotizacion/CotizacionUpdate.html'
    model = Cotizacion
    form_class = CotizacionUpdateForm
    context_object_name = 'cotizacion'
    pk_url_kwarg = 'pk'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['empresa'] = Empresa.objects.get(rfc=self.kwargs['rfc'])
        return context

    def get_success_url(self, **kwargs):
        cotizacion_kwargs = {
            'rfc': self.kwargs['rfc'],
            'pk': self.object.pk,
        }
        return reverse('gestor:detail_cotizacion', kwargs=cotizacion_kwargs)

# Productos Cotizados

class ProductoCotizadoUpdate(UpdateView):
    template_name='gestor/producto/ProductoCotizadoUpdate.html'
    model = ProductoCotizado
    form_class = ProductoCotizadoUpdateForm
    context_object_name = 'producto_cotizado'
    pk_url_kwarg = 'id'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['empresa'] = Empresa.objects.get(rfc=self.kwargs['rfc'])
        context['cotizacion'] = Cotizacion.objects.get(pk=self.kwargs['pk'])
        context['producto'] = self.object.producto
        return context

    def get_success_url(self, **kwargs):
        cotizacion_kwargs = {
            'rfc': self.kwargs['rfc'],
            'pk': self.kwargs['pk'],
        }
        return reverse('gestor:detail_cotizacion', kwargs=cotizacion_kwargs)

class ProductoCotizadoDelete(DeleteView):
    template_name='gestor/producto/ProductoCotizadoDelete.html'
    model = ProductoCotizado
    context_object_name = 'producto_cotizado'
    pk_url_kwarg = 'id'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['empresa'] = Empresa.objects.get(rfc=self.kwargs['rfc'])
        context['cotizacion'] = Cotizacion.objects.get(pk=self.kwargs['pk'])
        return context

    def get_success_url(self, **kwargs):
        cotizacion_kwargs = {
            'rfc': self.kwargs['rfc'],
            'pk': self.kwargs['pk'],
        }
        return reverse('gestor:detail_cotizacion', kwargs=cotizacion_kwargs)

class ProductoAdd(CreateView):
    template_name = 'gestor/producto/ProductoAdd.html'
    model = ProductoCotizado
    form_class = ProductoAddForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['empresa'] = Empresa.objects.get(rfc=self.kwargs['rfc'])
        context['cotizacion'] = Cotizacion.objects.get(pk=self.kwargs['pk'])
        context['producto'] = Producto.objects.get(id=self.kwargs['id'])
        return context

    def form_valid(self, form):
        form.instance.cotizacion = Cotizacion.objects.get(pk=self.kwargs['pk'])
        form.instance.producto = Producto.objects.get(id=self.kwargs['id'])
        return super().form_valid(form)

    def get_success_url(self, **kwargs):
        cotizacion_kwargs = {
            'rfc': self.kwargs['rfc'],
            'pk': self.kwargs['pk'],
        }
        return reverse('gestor:detail_cotizacion', kwargs=cotizacion_kwargs)

# Factura

class EmpresaFacturaList(SingleObjectMixin, ListView):
    template_name = 'gestor/factura/FacturaList(Empresa).html'
    model = Empresa
    pk_url_kwarg='rfc'
    context_object_name = 'empresa'
    paginate_by = 25

    def get(self, request, *args, **kwargs):
        self.object = self.get_object(queryset=Empresa.objects.all())
        return super().get(request, *args, **kwargs)

    def get_queryset(self):
        return self.object.cotizaciones.filter(is_factura=True)

class FacturaCreate(CreateView):
    template_name = 'gestor/factura/FacturaCreate.html'
    model = Factura
    form_class = FacturaCreateForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['empresa'] = Empresa.objects.get(rfc=self.kwargs['rfc'])
        context['cotizacion'] = Cotizacion.objects.get(pk=self.kwargs['pk'])
        return context

    def form_valid(self, form):
        form.instance.cotizacion = Cotizacion.objects.get(id=self.kwargs['pk'])
        form.instance.cotizacion.is_factura = True
        form.instance.cotizacion.save()
        return super().form_valid(form)

    def get_success_url(self, **kwargs):
        factura_kwargs = {
            'rfc': self.kwargs['rfc'],
            'id': self.object.id,
        }
        return reverse('gestor:detail_factura', kwargs=factura_kwargs)

class FacturaDetail(SingleObjectMixin, ListView):
    template_name = 'gestor/factura/FacturaDetail.html'
    model = Factura
    pk_url_kwarg='id'
    context_object_name = 'factura'
    paginate_by = 25

    def get(self, request, *args, **kwargs):
        self.object = self.get_object(queryset=Factura.objects.all())
        return super().get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['empresa'] = Empresa.objects.get(rfc=self.kwargs['rfc'])
        context['cotizacion'] = self.object.cotizacion
        context['archivos'] = self.object.factura_archivos.all()
        return context

    def get_queryset(self):
        return self.object.cotizacion.productos_cotizados.all()

class CotizacionGeneratePDF(View):
    def get(self, request, *args, **kwargs):
         template = get_template('gestor/cotizacion/CotizacionPDF.html')
         cotizacion = Cotizacion.objects.get(pk=self.kwargs['pk'])
         context = {
            'cotizacion': cotizacion,
            'empresa': cotizacion.empresa,
            'productos_cotizados': cotizacion.productos_cotizados.all(),
         }
         html = template.render(context)
         pdf = render_to_pdf('gestor/cotizacion/CotizacionPDF.html', context)
         if pdf:
             response = HttpResponse(pdf, content_type='application/pdf')
             filename = "Cotizacion_Empresa_%s_#_%d.pdf" %(cotizacion.empresa.rfc, cotizacion.id)
             content = "inline; filename='%s'" %(filename)
             download = request.GET.get("download")
             if download:
                 content = "attachment; filename='%s'" %(filename)
             response['Content-Disposition'] = content
             return response
         return HttpResponse("Not found")

class SignUpView(FormView):
    template_name = 'registration/signup.html'
    form_class = UserSignUpForm

    def form_valid(self, form):
        user = form.save(commit=False)
        user.save()
        return super().form_valid(form)

    def get_success_url(self, **kwargs):
        return reverse('gestor:login')

# Producto

class ProductoList(TemplateView):
    template_name = 'gestor/producto/ProductoList.html'

class ProductoCreate(CreateView):
    template_name = 'gestor/producto/ProductoCreate.html'
    model = Producto
    form_class = ProductoCreateForm

    def get_success_url(self, **kwargs):
        return reverse('gestor:detail_producto', kwargs={'id': self.object.id})

class ProductoUpdate(UpdateView):
    template_name='gestor/producto/ProductoUpdate.html'
    model = Producto
    form_class = ProductoCreateForm
    context_object_name = 'producto'
    pk_url_kwarg = 'id'

    def get_success_url(self, **kwargs):
        return reverse('gestor:detail_producto', kwargs={'id': self.object.id})

class ProductoDelete(DeleteView):
    template_name='gestor/producto/ProductoDelete.html'
    model = Producto
    context_object_name = 'producto'
    pk_url_kwarg = 'id'

    def get_success_url(self, **kwargs):
        return reverse('gestor:list_producto')
