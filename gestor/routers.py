from rest_framework import routers

from .viewsets import UserViewSet

from .viewsets import EmpresaViewSet
from .viewsets import ContactoViewSet
from .viewsets import EmpresaContactoViewSet
from .viewsets import ProductoViewSet
from .viewsets import EmpresaProductoViewSet

from .viewsets import CotizacionViewSet
from .viewsets import ProductoCotizadoViewSet
from .viewsets import FacturaViewSet

router = routers.DefaultRouter()

router.register(r'user', UserViewSet)

router.register(r'empresa', EmpresaViewSet)
router.register(r'contacto', ContactoViewSet)
router.register(r'producto', ProductoViewSet)

router.register(r'cotizacion', CotizacionViewSet)
router.register(r'producto_cotizado', ProductoCotizadoViewSet)
router.register(r'factura', FacturaViewSet)
