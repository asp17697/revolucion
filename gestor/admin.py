from django.contrib import admin

from .models import Empresa, Contacto
from .models import Producto, CatalogoClaveProducto
from .models import Cotizacion, Factura, ProductoCotizado

admin.site.register(Empresa)
admin.site.register(Contacto)

admin.site.register(Producto)
admin.site.register(CatalogoClaveProducto)

admin.site.register(Cotizacion)
admin.site.register(ProductoCotizado)
admin.site.register(Factura)
