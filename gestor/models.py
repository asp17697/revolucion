from django.db import models

from django.contrib.auth.models import User

from .utils import update_producto_cotizado, update_cotizacion

# Dictionaries
class CatalogoClaveProducto(models.Model):
    clave = models.IntegerField(primary_key=True)
    descripcion = models.CharField(max_length=20)

    def __str__(self):
        return '%d' % (self.clave)

#Datos Escenciales para el funcionamiento

class Empresa(models.Model):
    rfc = models.CharField(
        max_length=13,
        primary_key=True,
    )
    razon_social = models.CharField(max_length=40)
    correo = models.EmailField(max_length=40)
    telefono = models.CharField(max_length=22)
    BANCOS = (
        (1, 'Bancomer'),
        (2, 'Banamex'),
        (3, 'Banorte'),
        (3, 'IXE'),
        (0, 'N/A'),
    )
    banco = models.IntegerField(
        choices=BANCOS,
        default=0,
    )
    referencia_bancaria = models.IntegerField(default = -1)
    CUIDAD_ESTADO = (
        (1, 'CDMX/CDMX'),
        (2, 'Estado de México/Toluca'),
    )
    FORMAS_PAGO = (
        (0, 'Efectivo'),
        (1, 'Cheque nominativo'),
        (2, 'Transferencia electrónicade fondos'),
        (3, 'Por definir'),
    )
    forma_pago = models.IntegerField(
        choices=FORMAS_PAGO,
        default=0,
    )
    METODOS_PAGO = (
        (0, 'PUE'),
        (1, 'PPD'),
    )
    metodo_pago = models.IntegerField(
        choices=METODOS_PAGO,
        default=0,
    )
    TIPOS_COMPROBANTE = (
        (0, 'I'),
        (1, 'E'),
        (2, 'T'),
        (3, 'P'),
        (4, 'N'),
    )
    tipo_comprobante = models.IntegerField(
        choices=TIPOS_COMPROBANTE,
        default=0,
    )
    cuidad_estado = models.IntegerField(
        choices=CUIDAD_ESTADO,
        default=1,
    )
    cp = models.IntegerField(default=-1)
    direccion = models.CharField(
        max_length=20,
        blank=True, 
        null=True,
    )

    def __str__(self):
        return '%s' % self.rfc

class Contacto(models.Model):
    empresa = models.ForeignKey(
        Empresa,
        on_delete=models.CASCADE,
    )
    nombre = models.CharField(max_length=23)
    correo = models.EmailField(max_length=20)
    numero = models.CharField(max_length=20)
    PREFIJO_CHOICES = (
        (0, ''),
        (1, 'Sr.'),
        (2, 'Sra.'),
        (3, 'Lic.'),
        (4, 'Ing.'),
    )
    prefijo = models.IntegerField(
        choices=PREFIJO_CHOICES,
        default=0,
    )

    def __str__(self):
        return '%d[Nombre:%s]' % (self.id, self.nombre)

class Producto(models.Model):
    numero_parte = models.CharField(max_length=40)
    TIPOS_PRODUCTO = (
        ('Prd', 'Producto'),
        ('Srv', 'Servicio'),
    )
    tipo_producto = models.CharField(
        max_length=3,
        choices=TIPOS_PRODUCTO,
        default='Prd',
    )
    UNIDADES_MEDIDA = (
        ('H87', 'Pieza'),
        ('XBX', 'Caja'),
        ('O', 'Otro'),
        ('0', 'No Aplica'),
    )
    unidad_medida = models.CharField(
        max_length=3,
        choices=UNIDADES_MEDIDA,
        default='H87',
    )
    clave = models.ForeignKey(
        CatalogoClaveProducto,
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
    )
    ALIMENTO_IVA = 0
    SIN_ESPECIFICAR_IVA = 16
    TIPOS_IVA = (
        (ALIMENTO_IVA, 'ALIMENTO(0%)'),
        (SIN_ESPECIFICAR_IVA, 'S/A(16%)'),
    )
    tipo_iva = models.IntegerField(
        choices=TIPOS_IVA,
        default=SIN_ESPECIFICAR_IVA,
    )
    descripcion = models.TextField(
        max_length=100,
        blank=True,
    )


# Cotización

class Cotizacion(models.Model):
    empresa = models.ForeignKey(
        Empresa,
        on_delete=models.SET_NULL,
        blank=True, 
        null=True,
        related_name='cotizaciones',
    )
    usuario = models.ForeignKey(
        User,
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
    )
    referencia = models.CharField(
        max_length=20,
        blank=True, 
        null=True,
    )
    fecha = models.DateField(auto_now_add=True)
    contacto = models.ForeignKey(
        Contacto,
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
    )
    costo_operacion = models.DecimalField(
        max_digits=10,
        decimal_places=2,
        default=0.00,
    )
    monto_iva = models.DecimalField(
        max_digits=10,
        decimal_places=2,
        default=0.00,
    )
    subtotal = models.DecimalField(
        max_digits=10,
        decimal_places=2,
        default=0.00
    )
    total = models.DecimalField(
        max_digits=10,
        decimal_places=2,
        default=0.00
    )
    is_factura = models.BooleanField(default=False)

    def __str__(self):
        return '%d[Nombre:%s]' % (self.id,self.referencia)

    def save(self, *args, **kwargs):
        update_cotizacion(self)
        super(Cotizacion, self).save(*args, **kwargs)


# Cuando se cotize un producto, en costo debera de aparecer el costo del producto
# junto con el respectivo margen de ganancia
class ProductoCotizado(models.Model):
    cotizacion = models.ForeignKey(
        Cotizacion,
        on_delete=models.CASCADE,
        related_name='productos_cotizados',
    )
    producto = models.ForeignKey(
        Producto,
        on_delete = models.SET_NULL,
        null = True,
        blank = True,
    )
    provedor = models.ForeignKey(
        Empresa,
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
    )
    precio = models.DecimalField(
        max_digits=10,
        decimal_places=2,
        default=0.00,
    )
    costo = models.DecimalField(
        max_digits=10,
        decimal_places=2,
        default=0.00,
    )
    margen_ganancia = models.PositiveIntegerField(default=0)
    cantidad = models.PositiveIntegerField(default=1)
    monto_iva = models.DecimalField(
        max_digits=10,
        decimal_places=2,
        default=-1.00,
    )
    importe = models.DecimalField(
        max_digits=10,
        decimal_places=2,
        default=0.00,
    )
    descripcion = models.TextField(
        max_length=55,
        blank=True,
    )

    def __str__(self):
        return '%d[Nombre:%s][Cotizacion:%d]' % (self.id, self.nombre, self.cotizacion.id)

    def save(self, *args, **kwargs):
        update_producto_cotizado(self)
        super(ProductoCotizado, self).save(*args, **kwargs)
        cotizacion = self.cotizacion
        update_cotizacion(cotizacion)
        cotizacion.save()

    def delete(self, *args, **kwargs):
        cotizacion = self.cotizacion
        super(ProductoCotizado, self).delete(*args, **kwargs)
        update_cotizacion(cotizacion)
        cotizacion.save()


class Factura(models.Model):
    cotizacion = models.OneToOneField(
        Cotizacion,
        on_delete=models.CASCADE,
        related_name='factura',
    )
    firma = models.CharField(max_length=30)
    monto_pagado = models.DecimalField(
        max_digits=10,
        decimal_places=2,
        default=0.00,
    )
    ESTADOS = (
        (-1, 'Cancelada'),
        (0, 'En curso'),
        (1, 'Pagada'),
    )
    estado = models.IntegerField(
        choices=ESTADOS,
        default=0,
    )
    fecha_pago = models.DateField(
        blank=True, 
        null=True,
    )

    def __str__(self):
        return '%d[Cotizacion:%d][Empresa:%s]' % (self.id, self.cotizacion.id, self.cotizacion.empresa.rfc)

class FacturaArchivos(models.Model):
    factura = models.ForeignKey(
        Factura,
        on_delete=models.CASCADE,
        related_name='factura_archivos',
    )
    ESTADOS_FACTURA = (
        (0, 'No pagada'),
        (1, 'Pagada'),
        (2, 'Cancelada'),
    )
    estado_factura = models.IntegerField(
        choices=ESTADOS_FACTURA,
        default=0,
    )
